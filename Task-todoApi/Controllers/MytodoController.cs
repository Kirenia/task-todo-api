﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Task_todoApi.Models;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Task_todoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MytodoController : ControllerBase
    {

        private readonly MytodoContext _context;

        public MytodoController(MytodoContext context)
        {
            _context = context;


        }
        [HttpGet]
        public List<Mytodo> Task()
        {

            DbConections db = new DbConections();
            List<Mytodo> List = db.GetListTodo();

            return List;

        }
        [HttpGet("{id}")]
        public Mytodo todoItem(long id)
        {
            DbConections db = new DbConections();
            List<Mytodo> List = db.GetListTodo();
            Mytodo resultIdTodo = null;
            foreach (var item in List)
            {
                if (item.id == id)
                {
                    resultIdTodo = item;
                }

            }
            return resultIdTodo;

        }
        [HttpPost]
        public void addTodo(Mytodo todoItem)
        {

            DbConections db = new DbConections();
            db.addTodo(todoItem);

        }

        [HttpPut("{id}")]
        public string compleateTodo(Mytodo todoItem, int id)
        {
            string message = "";
            DbConections db = new DbConections();
            if (todoItem.id!=id)
            {
                message = "bad request";
              
                return message;
            }
            db.compleateTodo(todoItem,id);
            return message="ok";
        
          

        }
        [HttpDelete("{id}")]
        public string deleteTodo(int id)
        {
            string message = "";
            DbConections db = new DbConections();
           
            db.deleteTask(id);
            return message = "Deleted";

        }





    }
}
