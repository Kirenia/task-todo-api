﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task_todoApi.Models
{
    public class MytodoContext : DbContext
    {
        public MytodoContext(DbContextOptions<MytodoContext> options) : base(options )
        {

        }
        //public DbSet<Mytodo> MyListodo { get; set; }
        public DbSet<DbConections> MyListodo { get; set; }

    }
}
