﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task_todoApi.Models
{
    public class Mytodo
    {
        //private int id;
        //private string title;
        //private bool isCompleate;
        public long id { get; set; }
        public string title { get; set; }
        public bool Iscompleted { get; set; }

        public Mytodo(long id, string title, bool Iscompleted)
        {
            this.id = id;
            this.title = title;
            this.Iscompleted = Iscompleted;
        }

        

    }
}
