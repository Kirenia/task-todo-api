﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace Task_todoApi.Models
{
    public class DbConections
    {
        public List<Mytodo> Listodo = new List<Mytodo>();
        public Mytodo item;
        public DbConections()
        {


        }

        private SqlConnectionStringBuilder Connect()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "kire-sqldb.database.windows.net";
            builder.UserID = "kirenia";
            builder.Password = "Sebastian2016*";
            builder.InitialCatalog = "kiretododb";
            return builder;
        }
        public List<Mytodo> GetListTodo()
        {

            try
            {
                SqlConnectionStringBuilder builder = this.Connect();

                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {

                    connection.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("SELECT * FROM todoItems ");

                    String sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                int id = reader.GetInt32(0);
                                string title = reader.GetString(1);
                                bool IsCompleate = reader.GetBoolean(2);
                                item = new Mytodo(id, title, IsCompleate);
                                // Listodo = new List<Mytodo>();
                                Listodo.Add(item);

                                // Console.WriteLine("{0} {1} {2}", reader.GetInt32(0), reader.GetString(1), reader.GetBoolean(2) );
                            }
                            return Listodo;

                        }
                    }

                }
            }
            catch (SqlException e)
            {
                throw e;
            }

        }


        public void addTodo(Mytodo item)
        {

            try
            {
                SqlConnectionStringBuilder builder = this.Connect();

                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {

                    connection.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append($"INSERT INTO todoItems(title, completed) VALUES('{item.title}', '{item.Iscompleted}');");

                    string sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {

                        command.ExecuteNonQuery();

                    }
                }


            }

            catch (SqlException e)
            {
                throw e;
            }

        }



        public void compleateTodo(Mytodo item, int id)
        {

            try
            {
                SqlConnectionStringBuilder builder = this.Connect();

                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {

                    connection.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append($"UPDATE todoItems SET completed='{item.Iscompleted}' WHERE id='{id}'");

                    String sql = sb.ToString();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {

                        command.ExecuteNonQuery();

                    }



                }
            }
            catch (SqlException e)
            {
                throw e;
            }

        }

        public void deleteTask(int id)
        {
            try
            {
                SqlConnectionStringBuilder builder = this.Connect();

                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {

                    connection.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append($"DELETE FROM  todoItems where id='{id}'");

                    string sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {

                        command.ExecuteNonQuery();

                    }
                }

            }
            catch (SqlException e)
            {
                throw e;
            }


        }

    }
}


   

