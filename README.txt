Task - Todo API

Todo API using Azure and C#
Take all the principles we learnt this week and build a Todo application

    Write a C# API that can add, complete and get todos from a Database
    Don't delete records, we never delete from a database! (Right?!)
    You should host your app using a Docker container
    You may use either CosmosDB or SQL Server as a datastore
    You can use Postman to test your API
    If you�re feeling adventurous, you can build a small front-end too (Not required)

Submission

Submit your task in a document file. The text file should contain a link to your API endpoints for Adding, Completing and Getting todos from your database. Also specify the data structure required to add a todo.
I will test your API with Postman

Suggested structure for a Todo item:

{
    id: number
    title: string
    completed: boolean
}


Task - Todo API

Todo API using Azure and C#
Take all the principles we learnt this week and build a Todo application

    Write a C# API that can add, complete and get todos from a Database
    Don't delete records, we never delete from a database! (Right?!)
    You should host your app using a Docker container
    You may use either CosmosDB or SQL Server as a datastore
    You can use Postman to test your API
    If you�re feeling adventurous, you can build a small front-end too (Not required)

<----------------------------------------------------------------------------------------->
SITE URL
http://kire-api.northeurope.azurecontainer.io/api/Mytodo

Get todos

GET

http://kire-api.northeurope.azurecontainer.io/api/Mytodo

GET{id}

http://kire-api.northeurope.azurecontainer.io/api/Mytodo/5


<---------------------------------------------------------------------------------------->
Add todos
POST{id}
http://kire-api.northeurope.azurecontainer.io/api/Mytodo/

 {
      
        "title": "Read a book",
        "iscompleted": true
  }

<--------------------------------------------------------------------------------------------->
Complete todos 
PUT{id} Change the Iscomplete property to true;
http://kire-api.northeurope.azurecontainer.io/api/Mytodo/4

 {
        "id": 3,
        "title": "Read a book",
        "iscompleted": true
    }
<------------------------------------------------------------------------------------------->

DELETE{id}

http://kire-api.northeurope.azurecontainer.io/api/Mytodo/4

